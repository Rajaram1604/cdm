Class {meta::pure::profiles::doc.doc = 'A type for defining payments. Developers of FpML models are encouraged to make use of the SimplePayment type instead of this Payment type. In Transparency view, normally the payer and receiver party references are not used; however they may be provided if necessary for administrative activities such as Reporting Party Determination in FX.'} FpML::model::Payment extends FpML::model::PaymentBase
{
  {meta::pure::profiles::doc.doc = 'A reference to the party responsible for making the payments defined by this structure.'} payerPartyReference: FpML::model::PartyReference[0..1];
  {meta::pure::profiles::doc.doc = 'A reference to the account responsible for making the payments defined by this structure.'} payerAccountReference: FpML::model::AccountReference[0..1];
  {meta::pure::profiles::doc.doc = 'A reference to the party that receives the payments corresponding to this structure.'} receiverPartyReference: FpML::model::PartyReference[0..1];
  {meta::pure::profiles::doc.doc = 'A reference to the account that receives the payments corresponding to this structure.'} receiverAccountReference: FpML::model::AccountReference[0..1];
  {meta::pure::profiles::doc.doc = 'The currency amount of the payment.'} paymentAmount: FpML::model::NonNegativeMoney[0..1];
  {meta::pure::profiles::doc.doc = 'The payment date. This date is subject to adjustment in accordance with any applicable business day convention.'} paymentDate: FpML::model::AdjustableOrAdjustedDate[0..1];
  {meta::pure::profiles::doc.doc = 'A classification of the type of fee or additional payment, e.g. brokerage, upfront fee etc. FpML does not define domain values for this element.'} paymentType: FpML::model::PaymentType[0..1];
  {meta::pure::profiles::doc.doc = 'The information required to settle a currency payment that results from a trade.'} settlementInformation: FpML::model::SettlementInformation[0..1];
  {meta::pure::profiles::doc.doc = 'The value representing the discount factor used to calculate the present value of the cash flow.'} discountFactor: Decimal[0..1];
  {meta::pure::profiles::doc.doc = 'The amount representing the present value of the forecast payment.'} presentValueAmount: FpML::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'Can be used to reference the yield curve used to estimate the discount factor.'} href: String[0..1];
}
