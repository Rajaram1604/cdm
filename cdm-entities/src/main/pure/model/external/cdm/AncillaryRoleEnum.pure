Enum {meta::pure::profiles::doc.doc = 'Defines the enumerated values to specify the ancillary roles to the transaction. The product is agnostic to the actual parties involved in the transaction, with the party references abstracted away from the product definition and replaced by the AncillaryRoleEnum. The AncillaryRoleEnum can then be positioned in the product and the AncillaryParty type, which is positioned outside of the product definition, allows the AncillaryRoleEnum to be associated with an actual party reference.'} model::external::cdm::AncillaryRoleEnum
{
  {meta::pure::profiles::doc.doc = 'Specifies the party which determines additional disruption events.'} DisruptionEventsDeterminingParty,
  {meta::pure::profiles::doc.doc = 'Specifies the party which determines if dividends are extraordinary in relation to normal levels.'} ExtraordinaryDividendsParty,
  {meta::pure::profiles::doc.doc = 'Specifies the clearing organization (CCP, DCO) which the trade should be cleared.'} PredeterminedClearingOrganizationParty,
  {meta::pure::profiles::doc.doc = 'Specifies the party to which notice of a manual exercise should be given.'} ExerciseNoticeReceiverPartyManual,
  {meta::pure::profiles::doc.doc = 'Specifies the party to which notice of a optional early termination exercise should be given.'} ExerciseNoticeReceiverPartyOptionalEarlyTermination,
  {meta::pure::profiles::doc.doc = 'Specifies the party to which notice of a cancelable provision exercise should be given.'} ExerciseNoticeReceiverPartyCancelableProvision,
  {meta::pure::profiles::doc.doc = 'Specifies the party to which notice of a extendible provision exercise should be given.'} ExerciseNoticeReceiverPartyExtendibleProvision,
  {meta::pure::profiles::doc.doc = 'Specifies the party responsible for performing calculation agent duties as defined in the applicable product definition.'} CalculationAgentIndependent,
  {meta::pure::profiles::doc.doc = 'Specifies the party responsible for performing calculation agent duties associated with an optional early termination.'} CalculationAgentOptionalEarlyTermination,
  {meta::pure::profiles::doc.doc = 'Specifies the party responsible for performing calculation agent duties associated with an mandatory early termination.'} CalculationAgentMandatoryEarlyTermination,
  {meta::pure::profiles::doc.doc = 'Specifies the party responsible for deciding the fallback rate.'} CalculationAgentFallback
}
