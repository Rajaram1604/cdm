Class {meta::pure::profiles::doc.doc = 'Defines the strike price of an option.'} model::external::cdm::OptionStrike
[
  one_of_constraint: ((((($this.strikePrice->isNotEmpty() &&
  $this.strikeReference->isEmpty()) &&
  $this.referenceSwapCurve->isEmpty()) &&
  $this.averagingStrikeFeature->isEmpty()) ||
  ((($this.strikeReference->isNotEmpty() &&
  $this.strikePrice->isEmpty()) &&
  $this.referenceSwapCurve->isEmpty()) &&
  $this.averagingStrikeFeature->isEmpty())) ||
  ((($this.referenceSwapCurve->isNotEmpty() &&
  $this.strikePrice->isEmpty()) &&
  $this.strikeReference->isEmpty()) &&
  $this.averagingStrikeFeature->isEmpty())) ||
  ((($this.averagingStrikeFeature->isNotEmpty() &&
  $this.strikePrice->isEmpty()) &&
  $this.strikeReference->isEmpty()) &&
  $this.referenceSwapCurve->isEmpty())
]
{
  {meta::pure::profiles::doc.doc = 'Defines the strike of an option in the form of a price that could be a cash price, interestRate, or other types.'} strikePrice: model::external::cdm::Price[0..1];
  <<model::external::cdm::metadata.reference>> {meta::pure::profiles::doc.doc = 'Defines the strike of an option in reference to the spread of the underlying swap (typical practice in the case of an option on a credit single name swaps).'} strikeReference: model::external::cdm::FixedRateSpecification[0..1];
  {meta::pure::profiles::doc.doc = 'Defines the strike of an option when expressed by reference to a swap curve (Typically the case for a convertible bond option).'} referenceSwapCurve: model::external::cdm::ReferenceSwapCurve[0..1];
  {meta::pure::profiles::doc.doc = 'Defines an  option strike that is calculated from an average of observed market prices.'} averagingStrikeFeature: model::external::cdm::AveragingStrikeFeature[0..1];
}
